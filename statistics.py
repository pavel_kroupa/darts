import pandas as pd
import matplotlib.pyplot as plt
import glob
# import os

# arr = os.listdir('Export/Leg/*.csv')

all_data=pd.DataFrame()
names_df=pd.DataFrame()
closing_data=pd.DataFrame()
csv_files=glob.glob("Export/L*.csv")
csv_files.sort() # to get correct order

for file in csv_files[-6:]:
    df=pd.read_csv(file)
    df['date']=str(file.split('_')[2:])
    df_sel=df.iloc[:-1]
    # df_cl=df.tail(1)
    if df_sel['name'].str.contains('Miran|Martin').any():
        # all_data.append(row,ignore_index=True)
        names_df=names_df.append(df_sel,ignore_index=True)
        all_data=all_data.append(df,ignore_index=True)
    # closing_data=closing_data.append(df_cl, ignore_index=True) 

names=names_df['name'].unique()
res_dict={}
for name in names:
    res_dict[name]=[]
dates=all_data['date'].unique()
x_ax=[]
x_axn=[]
cavg_list=[]
ctxt_list=[]
for day in dates:
    # select date and remove nan columns :
    day_sel_df=all_data[all_data['date']==day].dropna(axis='columns')
    del day_sel_df['date'] # remove date column

    day_df=day_sel_df.iloc[:-1] # legs dataframe
    day_cl=day_sel_df.tail(1) # closing data frame

    cols=list(day_sel_df.columns)
    # cols.remove('date') # not to include in legs sequence
    #legs columns for current date
    pcols=cols[2:] # -1 to not include the date as latest column

    # day names:
    dnames=day_df['name'].unique()
    for leg in pcols:
        #legend preparation:
        prep_leg_unique=day+'_'+str('%02d' % int(leg))
        prep_leg=''+leg
        x_ax.append(prep_leg) # x_axis labels
        x_axn.append(prep_leg_unique)
        # short name who closed the leg:
        cl_cell=day_cl[leg].values[0]
        #getting long name:
        c_name=[n for n in dnames if cl_cell[:3] in n]
        # c_name=c_name[0]

        #avg score of one who closed leg:
        cavg=day_df[leg][day_df['name'].isin(c_name)].values[0]

        # lists of avg score and who closed loeg
        cavg_list.append(float(cavg))
        ctxt_list.append(cl_cell) # like (Mir(1))

    for name in names:
        if name in list(day_df['name']):
            srow=day_df[day_df['name']==name].iloc[:,2:]
            leg_list= srow.values.tolist()[0]
        else:
            leg_list=['Nan']*len(pcols)

        for score in leg_list:
            res_dict[name].append(float(score))


my_col_seq=['b', 'g', 'r', 'c', 'm', 'y', 'k', 'tab:orange','tab:blue','tab:red']
my_col_dict={'Pavel' : 'tab:blue',
                        'Miran' : 'tab:orange',
                        'Martin' : 'tab:green',
                        'Kačka' : 'tab:red',
                        'Helča' : 'm',
                        'Pavouk' : 'k',
                        'Martina' : 'y',
                        'Libor' : 'tab:green',
                        'Mirek H.' : 'xkcd:deep brown',
                        'Vraťka' : 'salmon',
                        'Míla' : 'turquoise',
                        'Karolínka' : 'yellowgreen'}



plt.figure(figsize=(18,5))
#plt.figure(figsize=(8,4))
for i, name in enumerate(res_dict.keys()):
    try:
        p_color=my_col_dict[name]
    except:
        p_color=my_col_seq[i]

    plt.plot(x_axn,res_dict[name], 'o-', color=p_color)



for i in range(len(ctxt_list)):
    plt.text(i,cavg_list[i],ctxt_list[i],fontsize=8, weight='bold',
             horizontalalignment='center',
             verticalalignment='bottom')
plt.legend(names)
plt.xticks(x_axn,x_ax)
#plt.xticks(rotation='vertical')
plt.grid('both')
plt.xlabel('day & leg')
plt.ylabel('avg score')
plt.tight_layout()

# print vertical lines:
x_dates=[n for n in x_axn if '_01' in n]
xv=[]
for d in x_dates:
    v_line=x_axn.index(d)
    plt.axvline(x=v_line-0.5,linewidth=1, color='k')
    xv.append(x_axn.index(d))
plt.xlim(left=-0.5,right=len(x_axn)-0.5)


#print dates in the middle of day:
xv.append(len(x_ax)) # to have last end point for x axis

#print(plt.gca().get_ylim())
ymin=plt.gca().get_ylim()[0]
ymax=plt.gca().get_ylim()[1]
y_date=ymin-(ymax-ymin)/12
#xl=plt.gca().xaxis.get_label()
#print(xl.get_position())
for i in range(len(xv)-1):
    x_pos=(xv[i]+xv[i+1])/2
    if i==0:
        print_date=dates[i][2:10]
    else:
        print_date=dates[i][6:10]
    plt.text(x_pos,y_date, print_date,ha='center',weight='bold')
plt.savefig('Export/Darts_statisctics',dpi=300)

plt.show()
