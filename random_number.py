import tkinter as tk
import random
#from tkinter import ttk
#import ttk  # import tkinter.ttk as ttk for Python 3
#s=ttk.Style()

#s.theme_use('clam')

class RandNum(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.title('Random number')
        self.geometry('{}x{}'.format(400, 600))
        self.bgcol='gray19'
        self.configure(background=self.bgcol)
        self.grid_columnconfigure(1, weight=1)
        self.grid_rowconfigure(3, weight=1)
        self.create_widgets()
        self.bcdict=self.best_checkout()
        #print(self.bcdict)
    
    def gen_rand_n(self,*args):
        rand_list=[]
        for i in range(5):
            n=random.randint(self.low.get(),self.high.get())
            rand_list.append(n)
        print(rand_list)
        self.rand_n.set(rand_list[0])
        try:
            self.best.set(self.bcdict[rand_list[0]])
        except:
            self.best.set('')
        return rand_list

    def create_widgets(self, *args):
        rand_button=tk.Button(self, text="Generate random", 
                                 pady=130, width = 18,
                                 font=("Helvetica", 36),
                                 command=lambda:self.gen_rand_n())
        rand_button.grid(row=3, column=0, columnspan=3)
        
        # label with random jumber:
        self.rand_n=tk.IntVar()
        self.rand_n.set(0)
        label=tk.Label(self, textvariable=self.rand_n,
                       font=("Helvetica", 160, 'bold'),
                       padx=10, pady=80, width=5,
                       bg=self.bgcol,
                       fg='white')
        label.grid(row=0, column=0, columnspan=3)
        
        
        # label with best checkout:
        self.best=tk.StringVar()
        self.best.set('')
        best_label=tk.Label(self, textvariable=self.best,
                       font=("Helvetica", 16, 'bold'),
                       padx=10, pady=0, width=10,
                       bg=self.bgcol,
                       fg='gray')
        best_label.grid(row=1, column=1)
        
        
        # low limit entry:
        self.low=tk.IntVar()
        self.low.set(40)
        low_entry=tk.Entry(self, textvariable=self.low,
        font=("Helvetica", 20, 'bold'),
        width=5)
        low_entry.grid(row=2, column=0)
        
        # high limit entry:
        self.high=tk.IntVar()
        self.high.set(98)
        low_entry=tk.Entry(self, textvariable=self.high,
        font=("Helvetica", 20, 'bold'),
        width=5)
        low_entry.grid(row=2, column=2)

    def best_checkout(*argd):
        bc={
        41 : '9+D16',
        42 : '10+D16',
        43 : '3+D20',
        44 : '4+D20',
        45 : '13+D16',
        46 : '6+D20',
        47 : '7+D20',
        48 : '16+D16',
        49 : '17+D16',
        50 : '18+D16',
        51 : '19+D16',
        52 : '20+D16',
        53 : '13+D20',
        54 : '14+D20',
        55 : '15+D20',
        56 : '16+D20',
        57 : '17+D20',
        58 : '18+D20',
        59 : '19+D20',
        60 : '20+D20',
        61 : 'T15+D8',
        62 : 'T10+D16',
        63 : 'T13+D12',
        64 : 'T16+D8',
        65 : 'T19+D4',
        66 : 'T14+D12',
        67 : 'T17+D8',
        68 : 'T20+D4',
        69 : 'T19+D6',
        70 : 'T18+D8',
        71 : 'T13+D16',
        72 : 'T16+D12',
        73 : 'T19+D8',
        74 : 'T14+D16',
        75 : 'T17+D12',
        76 : 'T20+D18',
        77 : 'T19+D10',
        78 : 'T18+D12',
        79 : 'T19+D11',
        80 : 'T20+D10',
        81 : 'T19+D12',
        82 : 'D25+D16',
        83 : 'T17+D16',
        84 : 'T20+D12',
        85 : 'T15+D20',
        86 : 'T18+D16',
        87 : 'T17+D18',
        88 : 'T20+D14',
        89 : 'T19+D16',
        90 : 'T20+D15',
        91 : 'T17+D20',
        92 : 'T20+D16',
        93 : 'T19+D18',
        94 : 'T18+D20',
        95 : 'T19+D19',
        96 : 'T20+D18',
        97 : 'T19+D20',
        98 : 'T20+D19',
        99 : 'no',
        100 : 'T20+D20'
        }
        
        return bc 


app = RandNum()
app.mainloop()