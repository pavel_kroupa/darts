import tkinter as tk
import tkinter.font as font
# import matplotlib.pyplot as plt
# from matplotlib.figure import Figure

import dart_board
import dart_functions

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

class MyGUI(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.title('Darts')
        self.geometry('{}x{}'.format(1300, 750))
        Input_Frame=tk.Frame(self, bg='cyan',padx=3, pady=3)
        Result_Frame = tk.Frame(self, bg='white',padx=3, pady=3)			
        Analysis_Frame=tk.Frame(self, bg='blue',padx=3, pady=3)
        # Leg_Frame=tk.Frame(self, bg='green',padx=3, pady=3)
        Board_Frame=tk.Frame(self, bg='yellow',padx=3, pady=3)
        
		
		# layout all of the main containers
        Input_Frame.grid(row=0, column=0, sticky="nsew")
        Result_Frame.grid(row=1, column=0, sticky ="nsew")
        Analysis_Frame.grid(row=2, column=0, sticky ="nsew")
        # Leg_Frame.grid(row=3, column=0, sticky ="sew")
        Board_Frame.grid(row=0, column=1, rowspan=4, sticky = 'e')
        
        self.Input_Frame=Input_Frame
        self.Result_Frame=Result_Frame
        self.Analysis_Frame=Analysis_Frame
        # self.Leg_Frame=Leg_Frame
        self.Board_Frame=Board_Frame
        
        self.grid_columnconfigure(1, weight=1) # to stretch items placed in row 0
        self.grid_columnconfigure(0, weight=0)
        self.grid_rowconfigure(1, weight=0)
        self.grid_rowconfigure(2, weight=1)
        self.grid_rowconfigure(3, weight=0)
        
        Input_Frame.grid_columnconfigure(0, weight=0)
        Input_Frame.grid_columnconfigure(1, weight=0)
        Input_Frame.grid_columnconfigure(2, weight=1)
        Input_Frame.grid_columnconfigure(4, weight=1)
        
        # Result_Frame.grid_columnconfigure(0, weight=0)
        # Result_Frame.grid_columnconfigure(7, weight=0)
        
        Analysis_Frame.grid_columnconfigure(0, weight=1)
        Analysis_Frame.grid_rowconfigure(0, weight=1)
        
        myFont = font.Font(family='Helvetica', size=14, weight='bold')
        self.myFont=myFont
        myFont_small = font.Font(family='Helvetica', size=8, weight='bold')
        self.myFont_small=myFont_small
        
        self.define_input_frame() # creates widgets in Input Frame  
        self.define_result_frame() # creates widgets in Input Frame
        # self.define_leg_frame()
        
         # indicator:
        self.indicator=tk.Label(Result_Frame, text=' \u25C0 ', 
                                width=3,anchor='s',font=("Helvetica", 14),
                                bg='red',fg='white') 
        self.new_game_init() # needed to generate the table for Resutlts frame
        
        self.plot_dart_board()
        
    def plot_dart_board(self):
        Board_Frame=self.Board_Frame
        # plot dartboard:
        fig = dart_board.fig_board()
        # self.ax=fig.axes[0]
        self.board_fig=fig
        # self.doard_ax=ax
        canvas = FigureCanvasTkAgg(fig, Board_Frame)
        canvas._tkcanvas.pack(fill='both',expand=True)
        # canvas.get_tk_widget().grid(row = 0,column = 0, sticky='nsew')
        self.canvas=canvas
        canvas.mpl_connect('button_press_event',self.callback)
        self.points=[]
        self.hit_texts=[]
   
    def define_input_frame(self):
        '''Input frame widgets definition'''
        Input_Frame=self.Input_Frame
        self.init=dart_functions.read_ini()
        player_num_list=range(1,9)
        self.player_num=tk.StringVar(self)
        # player number: from ini file
        self.player_num.set(self.init['Player_num'][0])
        popup_player_num=tk.OptionMenu(Input_Frame,self.player_num,*player_num_list)
        popup_player_num.config(width=4)
        popup_player_num.grid(row=1, column=1)
        self.player_num.trace('w',self.create_name_list)
        
        # Check button to select Team play
        self.team_var=tk.IntVar()
        #self.team_var.set(0)
        c1=tk.Checkbutton(Input_Frame, text='Teams',
        variable=self.team_var, onvalue=1, offvalue=0, 
        command=self.create_name_list())
        self.team_var.trace('w',self.create_name_list)
        c1.grid(row=2, column=1)
        
        
        self.create_name_list()
        # top input frame widgets:
        label=tk.Label(self.Input_Frame, text="Game type:", #font=myFont,
                       padx=10, pady=5, width=8)
        label.grid(row=0, column=0)
        
        label=tk.Label(self.Input_Frame, text="Players:", #font=myFont,
                       padx=10, pady=5,width=8)
        label.grid(row=1, column=0)
        
        NewGame_button=tk.Button(self.Input_Frame, text="New Game", #font=myFont,
                                 pady=5,command=self.new_game_init, width=9)
        NewGame_button.grid(row=2,column=0)
        
        Exit_button=tk.Button(self.Input_Frame, text="Exit", pady=5,
                              command=self.destroy, width=9)
        Exit_button.grid(row=3,column=0)
        
        undo_button=tk.Button(self.Input_Frame, text="Undo \n \u21b6", pady=10,
                              command=lambda: self.undo(self.data), width=6,
                              font=("Helvetica", 16))
        undo_button.grid(row=0,column=4,rowspan=4)
        
        redo_button=tk.Button(self.Input_Frame, text="Redo \n \u21e8", pady=10,
                              command=lambda: self.redo(self.data), width=6,
                              font=("Helvetica", 16))
        redo_button.grid(row=0,column=5,rowspan=4)
 
        
        # game selection list:
        game_list=['501','301', 'Cricket']
        self.game=tk.StringVar(self)
        self.game.set(self.init['GameType'][0])
        popop_game_list=tk.OptionMenu(self.Input_Frame, self.game, *game_list)
        popop_game_list.config(width=4)
        popop_game_list.grid(row=0, column=1)
        self.game.trace('w', self.change_game)
            
    
    def create_name_list(self, *args):
        Input_Frame=self.Input_Frame
        #print(self.player_num.get())
       # name selection list:
        pnum=int(self.player_num.get())
        # name list from ini file:
        name_list=self.init['Player'] 
        # name_list=['', 'Pavel', 'Miran', 'Kačka','Martin','Pavouk','Martina', 'Libor']
        
        try:
            for popup_list in self.popup_lists:
                popup_list.destroy()        
        except:
            pass
        self.names=[]
        self.popup_lists=[]
        for n in range(pnum): # drop down lists
            col=2
            pnext_col=int((pnum+1)/2)
            if n>(pnext_col-1): # player above 4
                vp=n-pnext_col
                col=3 # to move 5th player to next column
            else:
                vp=n
           
            name=tk.StringVar(self)
          
            if self.team_var.get()==1:
                name_list2=[]
                for item in name_list:
                    new_name='T'+str(vp+1)+': '+item
                    name_list2.append(new_name)
            else:
                name_list2=name_list
        
            
            if n<2: # show only 1st two names
                name.set(name_list2[n+1])
            else:
                name.set(name_list2[0])
            popop_name_list=tk.OptionMenu(Input_Frame, name, *name_list2)
            popop_name_list.config(width=7)
            
          
            popop_name_list.grid(row=vp, column=col)#, sticky='ew')
            name.trace('w', self.change_player)
            self.popup_lists.append(popop_name_list)
            self.names.append(name)
        
    def define_result_frame(self):
        '''Results frame Labels definition:''' 
        Result_Frame=self.Result_Frame
        myFont=self.myFont
        
        # Check button to show legs statisctics
        self.legs_stat=tk.IntVar()
        c2=tk.Checkbutton(Result_Frame, text='show legs avg',
        variable=self.legs_stat, onvalue=1, offvalue=0, 
        command=lambda: self.show_legs_stat())
        self.legs_stat.trace('w',self.show_legs_stat)
        c2.grid(row=0, column=0,columnspan=3)
        
        self.dart=tk.StringVar()
        self.dart.set('Dart: 1')
        dart_Label=tk.Label(Result_Frame, textvariable=(self.dart), 
                            font=myFont)
        dart_Label.grid(row=1, column=0, columnspan=3)    
        
        
        self.cround=tk.StringVar()
        self.cround.set('Round: 1')
        round_Label=tk.Label(Result_Frame, textvariable=(self.cround), 
                             width=10,font=myFont)
        round_Label.grid(row=1, column=4)
        
        # currentl player display
        self.cplayer=tk.StringVar()
        cplayer_Label=tk.Label(Result_Frame, textvariable=self.cplayer, 
                             font=("Helvetica", 24),
                             fg='black', pady=8, width=12)
        cplayer_Label.grid(row=0, column=2, columnspan=5)
        
        # above table labels:
        # round score label:
        Lrscore=tk.Label(Result_Frame, text='Round score', 
                             font=myFont,relief=tk.RIDGE, width=12,
                             bg='black',fg='white')
        Lrscore.grid(row=2, column=3)
        
        # name Label:
        Lname=tk.Label(Result_Frame, text='Player', 
                           font=myFont,relief=tk.RIDGE, width=16,
                           bg='black',fg='white')
        Lname.grid(row=2, column=4)

        # legs Label:
        Llabel=tk.Label(Result_Frame, text='Legs', 
                            font=myFont,relief=tk.RIDGE, width=5, 
                            bg='black',fg='white')
        Llabel.grid(row=2, column=5)
                 
        # score Label:
        Lscore=tk.Label(Result_Frame, text='Score', 
                            font=myFont,relief=tk.RIDGE, width=8, 
                            bg='black',fg='white')
        Lscore.grid(row=2, column=6)
        
        # iterate whrough table rows (1 row for each player):
        snames=self.selected_names()
        # table=self.table   
        self.hit={}
        self.rscore={}
        self.name={}
        self.legs={}
        self.score={}
        # print(snames)
        try:
            for label in self.table_labels:
                label.destroy()
        except:
            pass
        
        
        self.table_labels=[]
        for i in range(len(snames)):      
            # print(self.selected_names[i])
            prow=i+3
            # iterate through hits:
            for h in range(3):
                self.hit[i,h]=tk.StringVar()
                hit_Label=tk.Label(Result_Frame, textvariable=self.hit[i,h], 
                                    font=myFont, relief=tk.RIDGE, 
                                    width=4,fg='gray')
                hit_Label.grid(row=prow, column=h)
                self.table_labels.append(hit_Label)
            
            # round score:
            self.rscore[i]=tk.StringVar()
            rscore_Label=tk.Label(Result_Frame, textvariable=self.rscore[i], 
                                 font=myFont,relief=tk.RIDGE, width=12)
            rscore_Label.grid(row=prow, column=3)
            self.table_labels.append(rscore_Label)
            
            # name:
            self.name[i]=tk.StringVar()
            # self.name[i].set(row['name'])
            name_Label=tk.Label(Result_Frame, textvariable= self.name[i],
                               font=myFont,relief=tk.RIDGE, width=16,
                               bg='white')
            name_Label.grid(row=prow, column=4)
            self.table_labels.append(name_Label)
            
            # legs:
            self.legs[i]=tk.StringVar()
            legs_Label=tk.Label(Result_Frame, textvariable=self.legs[i], 
                                font=myFont,relief=tk.RIDGE, width=5, 
                                bg='blue',fg='white')
            legs_Label.grid(row=prow, column=5)
            self.table_labels.append(legs_Label)
                   
            # score:
            self.score[i]=tk.StringVar()
            score_Label=tk.Label(Result_Frame, textvariable=self.score[i], 
                                font=myFont,relief=tk.RIDGE, width=8, 
                                bg='blue',fg='white')
            score_Label.grid(row=prow, column=6)
            self.table_labels.append(score_Label)
            
        # indicator:
#        self.indicator=tk.Label(Result_Frame, text=' \u25C0 ', 
#                              width=3,anchor='s',font=("Helvetica", 12),
#                              bg='red',fg='white') 
    
    def define_leg_frame(self):
        

        try:
            for label in self.leg_table_labels:
                    label.destroy()
            self.Leg_Frame.destroy()
        except:
            pass
            
         
        Leg_Frame=tk.Frame(self, bg='green',padx=3, pady=3)
        Leg_Frame.grid(row=3, column=0, sticky ="sew")
        self.Leg_Frame=Leg_Frame
        
        myFont_small=self.myFont_small
        Leg_Frame=self.Leg_Frame
        try:
            for label in self.leg_table_labels:
                label.destroy()
        except:
            pass
        
        leg_table_labels=[] # initiation to be able to destroy all labels
         # Header Label:
        Lname=tk.Label(Leg_Frame, text='Player', 
                           font=myFont_small,relief=tk.GROOVE, width=10,
                           borderwidth=1,
                           bg='white',fg='black')
        Lname.grid(row=0, column=0)
        leg_table_labels.append(Lname)
                 
        for i in range (1,16,1): # more 
            # Header leg labels:
            if i<12:    # create only 11 labels for legs:
                Leg_label=tk.Label(Leg_Frame, text='leg '+str(i), 
                               font=myFont_small,relief=tk.GROOVE,
                               borderwidth=1,
                               width=6,bg='white',fg='black')
                Leg_label.grid(row=0, column=i)
            leg_table_labels.append(Leg_label)
        
        
        snames=self.selected_names()
        self.leg_score={}
        self.lname={}
        for n in range(len(snames)+1):   #+1 for closing round
            self.lname[n]=tk.StringVar()
            name_Label=tk.Label(Leg_Frame, textvariable=self.lname[n],
                               font=myFont_small,relief=tk.GROOVE, 
                               borderwidth=1,width=10,
                               bg='white',fg='black')
            name_Label.grid(row=n+1, column=0)
            leg_table_labels.append(name_Label)
            for i in range (1,16,1):
                # legs filling:
                self.leg_score[n,i]=tk.StringVar()
                
                # create only 11 labels for legs:
                if i<12:    
                    Leg_label=tk.Label(Leg_Frame, 
                                           textvariable=self.leg_score[n,i], 
                               font=myFont_small,relief=tk.GROOVE, 
                               borderwidth=1,
                               width=6,bg='white',fg='black')
                    Leg_label.grid(row=n+1, column=i)
                    leg_table_labels.append(Leg_label)
        self.leg_table_labels=leg_table_labels
            
    def update_table(self, table):
        # myFont=self.myFont
        # Result_Frame=self.Result_Frame

        dart=int(self.data['dart'].tail(1))+1
        self.dart.set('Dart: '+str(dart))
        self.dartn=dart
        
        cround=int(max(table['round']))
        self.cround.set('Round: '+str(cround))
        
        current_player=dart_functions.get_current_player(self.data)
        self.cplayer.set(current_player)
        
        
        # iterate whrough table rows (1 row for each player):
        for index, row in table.iterrows():
            prow=index+3
            # iterate through hits:
            for h in range(3):
                hit=row['hit'+str(h+1)]
                self.hit[index,h].set(hit)
            
            # round score:
            self.rscore[index].set(int(row['rscore']))
                            
            # name:
            self.name[index].set(row['name'])   
            
            # indicator
            if row['name']==current_player:
                self.indicator

                self.indicator.grid(row=prow, column=7)
            
            
            # # legs:
            self.legs[index].set(int(row['legs']))
            #  score:
            self.score[index].set(int(row['score']))

    def update_leg_table(self, leg_table):      
        # iterate whrough table rows (1 row for each player):
        nlegs=len(leg_table.keys())
        for index, row in leg_table.iterrows():
            self.lname[index].set(row['name'])
            for leg in range(1, nlegs,1):
                self.leg_score[index,leg].set(row[leg])   
                
            
    def callback(self,event):
        # self.plot_hit(event.xdata, event.ydata)
        hit=dart_functions.get_hit(event.xdata, event.ydata)
        self.hit_str=hit
        self.data=dart_functions.add_row(self,hit,self.data)
        dart_board.plot_hit(self,event.xdata, event.ydata)
        dart_functions.sound_play(self)
        self.table=dart_functions.gen_table(self.data)
        self.update_table(self.table)
        if self.dartn==1:
            self.chart_plot(self.data)  
            self.leg_table=dart_functions.gen_leg_table(self.data)
            
            try:
                self.update_leg_table(self.leg_table)
            except:pass
            dart_board.remove_hits(self)
            self.board_fig_backup=self.board_fig
            self.points=[]
            self.hit_texts=[]
        return hit
    
    
    def chart_plot(self,df):
         # update chart:
        fig2 = dart_board.fig_hits(df)   
        canvas2 = FigureCanvasTkAgg(fig2, self.Analysis_Frame)
        # canvas.draw()
        canvas2.get_tk_widget().grid(row = 0,column = 0, sticky='nsew')
        # canvas2._tkcanvas.pack(fill='both',expand=True)
        # self.canvas=canvas

    def undo(self,df):
        self.data, self.backup=dart_functions.drop_last_row(self)
        table=dart_functions.gen_table(self.data)
        self.update_table(table)
        self.chart_plot(self.data)
        dart_board.remove_1hit(self)
    
    def redo(self,df):
        backup=self.backup
        self.data=dart_functions.restore_row(df,backup)
        table=dart_functions.gen_table(self.data)
        self.update_table(table)
        self.chart_plot(self.data)
    
    def selected_names(self):
        selected_name_list=[]
        for name in self.names:
            # print(name.get())
            sel_name=name.get()
            if len(sel_name)>0:
                selected_name_list.append(sel_name)    
        return selected_name_list
    
    def new_game_init(self):
        selected_name_list=self.selected_names()
        self.selected_name_list=selected_name_list
        dart_functions.sound_play_start_game()
        game_type=self.game.get()
        self.data=dart_functions.init_data(selected_name_list,game_type)
        self.table=dart_functions.gen_table(self.data)
        self.define_result_frame()
        # self.define_leg_frame()
        self.update_table(self.table)
        # plot empty chart:
        self.chart_plot(self.data)
        self.show_legs_stat(self)   
            
    def change_game(self,*args):
        print(self.game.get())
    
    def change_player(self, *args):
        # print(self.names[0].get())
        return
    
    def show_legs_stat(self, *args):
        if self.legs_stat.get()==1:
            print('show')
            # self.Leg_Frame.hide()
            self.define_leg_frame()
            self.leg_table=dart_functions.gen_leg_table(self.data)
            self.update_leg_table(self.leg_table)
        else:
            print('hide')
            try:
                for label in self.leg_table_labels:
                    label.destroy()
                self.Leg_Frame.destroy()
            except:
                pass
            
              
           

app = MyGUI()
app.mainloop()
