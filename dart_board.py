import numpy as np
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
import dart_functions
import pandas as pd

diam={'c1':0.08, 
	'c2': 0.17,
	't1': 0.5,
	't2': 0.6,
	'd1': 0.9,
	'd2': 1}

circ_points=2000 # points around the circle

betal=np.linspace(0, 2*np.pi, circ_points)
beta=np.linspace(0, 2*np.pi, circ_points)
line=[]
for i in np.arange(0,20,2):
	bs=int((circ_points*i/20)+(circ_points/20/2)) # beta start point
	be=int((circ_points*(i+1)/20)+(circ_points/20/2)) # beta end point
	betal[bs:be]=np.NAN
	

	
for i in np.arange(20):
	loc=int((circ_points*i/20)+(circ_points/20/2))
	line.append(beta[loc])



def fig_board():
    
    
    fig=Figure(figsize=(12,12), dpi=100)
    ax=fig.add_subplot(1,1,1, projection='polar')
    ax.fill_between(beta, diam['c1'], diam['c2'], color="green")
    ax.fill_between(beta, diam['c1'],color="red")
    
    ax.fill_between(beta, diam['c2'], diam['t1'], color="black")
    ax.fill_between(betal, diam['c2'], diam['t1'], color="white")
    
    ax.fill_between(beta, diam['d1'], diam['d2'], color="red")
    ax.fill_between(betal, diam['d1'], diam['d2'], color="green")
    
    ax.fill_between(beta, diam['t2'], diam['d1'], color="black")
    ax.fill_between(betal, diam['t2'], diam['d1'], color="white")
    
    
    ax.fill_between(beta, diam['t1'], diam['t2'], color="red")
    ax.fill_between(betal, diam['t1'], diam['t2'], color="green")
    
    ax.grid(b=None)
    ax.axis('off')
    for i in np.arange(20):
    	ax.plot([line[i],line[i]],[diam['c2'], diam['d2']],color='gray')
    for d in diam.keys():
    	ax.plot(beta,np.full(circ_points,diam[d]), color='gray')
    
    centnum=np.linspace(0, 2*np.pi, 20, endpoint= False)
    
    labels=(6, 13, 4, 18, 1, 20, 5, 12, 9, 14, 11, 8, 16, 7, 19, 3, 17, 2, 15, 10)
    for label, fi in enumerate(centnum):
    	#print(fi,label)
    	if fi>np.pi:
    		fip=fi+np.pi
    	else: fip=fi
    	ax.text(fi,1.1,labels[label],va='center', ha='center',
              rotation=fip*57-90,
              size=18)
    # outer ring
    ax.plot(beta,np.full(circ_points,1.15), color='gray')
    # fig.tight_layout()
    fig.subplots_adjust(left=-0, bottom=-0.0, right=1, top=1,
                    wspace=0, hspace=-0)
    return fig

def fig_hits(df):
    names=dart_functions.names_sequence(df)
    fig=Figure(figsize=(5,3), dpi=100)
    ax=fig.add_subplot(1,1,1)
    my_col_seq=['b', 'g', 'r', 'c', 'm', 'y', 'k', 'tab:orange','tab:blue','tab:red']
    my_col_dict={'Pavel' : 'tab:blue',
                            'Miran' : 'tab:orange',
                            'Martin' : 'tab:green',
                            'Kačka' : 'tab:red',
                            'Helča' : 'm',
                            'Pavouk' : 'k',
                            'Martina' : 'y',
                            'Libor' : 'tab:green',
                            'Mirek H.' : 'xkcd:deep brown',
                            'Vraťka' : 'salmon',
                            'Míla' : 'turquoise',
                            'Karolínka' : 'yellowgreen',
                            'Ozgun' : 'magenta',
                            'Cihan' : 'darkred'}
    #data preparation:
    last_row=df.tail(1)  
    cleg=last_row['cleg'].values[0]
    cplayer=dart_functions.get_current_player(df)
    my_legend=[]
    # generation of round hit list per player:
    for i, player in enumerate(names):
        
        ds=pd.DataFrame()
        rmax=int(df['round'][df['name']==player][df['cleg']==cleg].max())
        for r in range(1, rmax+1, 1):
            df2=df[df['name']==player]
            df3=df2[df2['cleg']==cleg] # only select score from current leg
            srow=df3[df3['round']==r].tail(1)
            hit1=srow['hit1'].values[0]
            # append values only after 1st dart:
            if len(hit1)>0:
                ds=ds.append(srow,ignore_index=True)

        # average hit:      
        if rmax>0: # plot only 1st round data, no init data
            
            if cplayer==player and len(hit1)==0:
                rmax=rmax-1 # not to count current round before the 1st hit
            
            try:
                ssum=ds['rscore'].sum()
                avghit=ssum/rmax
                my_legend.append('avg: '+'%4.1f' %avghit +' ' +player)
                # print(my_legend)
                # plotting for each player:
                y_ax=ds['rscore'][ds['name']==player][ds['round']>0]
                x_ax=list(ds['round'][ds['name']==player][ds['round']>0])              
                player=player.split(': ')[-1] # to get color for team play
                try:
                    p_color=my_col_dict[player]
                except:
                    p_color=my_col_seq[i]
            
                ax.plot(x_ax,y_ax,'-o', color=p_color)
                # print(y_ax,x_ax)
                ax.legend(my_legend)
                
            except: 
                ssum=0

            
    ax.xaxis.set_ticks(list(range(1,20)))
    ax.grid()
    ax.set_ylim(ymin=0,ymax=None)
    ax.set_xlim(0,16)
    ax.set_xlabel('round')
    ax.set_ylabel('score')
    fig.set_tight_layout(True)
    return fig

def plot_hit(self, fi,dist):
    # Board_Frame=self.Board_Frame
    dart=self.dartn
    figp=self.board_fig

    ax=figp.axes[0] # getting axes from figure
    my_col=['c','tab:blue','m','tab:orange']

    ax.plot(fi,dist,'o',color=my_col[dart-1]) 
    
    # append point to previous points (reseted when hit =1 in update_table)
    self.points.append(ax.lines[-1])
    # polar coordinates:
    fi_h=[2.36,2.44, 2.53]
    dist_h=[1.49,1.39,1.299]
    ax.text(fi_h[dart-1],dist_h[dart-1],self.hit_str,fontsize=20, fontweight='bold')
    self.hit_texts.append(ax.texts[-1])
    self.canvas.draw()
    
    
def remove_hits(self):
    # removing last points:
    figp=self.board_fig
    ax=figp.axes[0]
    # print(self.points)
    
    for p in self.points: 
        ax.lines.remove(p)
    for t in self.hit_texts: 
        ax.texts.remove(t)
    self.canvas.draw()
    
def remove_1hit(self):
    # removing 1 last point:
    figp=self.board_fig
    ax=figp.axes[0]
    
    ax.lines.remove(self.points[-1])
    ax.texts.remove(self.hit_texts[-1])
    self.canvas.draw()
    self.points=self.points[:-1]
    self.hit_texts=self.hit_texts[:-1]
