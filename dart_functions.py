import numpy as np
import pandas as pd
from pygame import mixer
from datetime import datetime
import os, random

mixer.init()

init_score=int(501)


def init_data(players,game_type):
    # players=['Pavel', 'Miran']
    try:
        init_score=int(game_type)
    except: return

    # Initialize dataframe:
    df=pd.DataFrame()
    for player in players:
        new_row={'name':player,'cleg': int(1),'legs': int(0),'round': int(0),
                 'dart': 0, 'score':init_score,'player_score' :init_score,
                 'pscore':init_score,'rscore':int(0),
             'hit1':'','hit2': '','hit3':'', 'next_player': True}
        df=df.append(new_row,ignore_index=True)

    return df

def read_ini():
    with open('darts.ini', 'r', encoding='utf-8') as f:
        content=f.readlines()
    init={}
    for line in content:
        if line[0]=='#': # skip comment line
            pass
        # reading "noraml" lines:
        else:
            label, val = line.rstrip('\r\n ').split('=')
            try:
                if len(init[label])>0:
                    my_list.append(val)
            except:
                my_list=[]
                my_list.append(val)

            init[label]=my_list
    return init

def gen_table(df):
    names=names_sequence(df)
    table=pd.DataFrame()
    # print(names)
    for name in names:
        new_row=df[df['name']==name].tail(1) # last row with given name
        table=table.append(new_row,ignore_index=True)

    table=table[['hit1','hit2','hit3','rscore','legs','round','dart','name','score']]

    return table

def get_hit(fi, dist):
    diam={'c1':0.08,
        'c2': 0.17,
        't1': 0.5,
        't2': 0.6,
        'd1': 0.9,
        'd2': 1}

    centnum=np.linspace(0, 2*np.pi/20*21, 21, endpoint= False)
    labels=(6, 13, 4, 18, 1, 20, 5, 12, 9, 14, 11, 8, 16, 7, 19, 3, 17, 2, 15, 10,6)
    num=labels[(abs(centnum-fi)).argmin()]
    # Bull
    if dist<diam['c1']:
        hit='D25'
    # 25 ring:
    elif dist>diam['c1'] and dist<diam['c2']:
        hit='25'
      #   
    elif (dist>diam['c2'] and dist<diam['t1']) or (
            dist>diam['t2'] and dist<diam['d1']):
        hit=str(num)
    elif dist>diam['t1'] and dist<diam['t2']:
        hit=('T'+str(num))
    elif dist>diam['d1'] and dist<diam['d2']:
        hit=('D'+str(num))

    else: hit=str(0)

    return hit

def get_current_player(df):
    last_row=df.tail(1)
    prev_player=last_row['name'].values[0]
    if last_row['next_player'].values[0]==True:
        names=names_sequence(df) # sorted names for this leg
        position=names.index(prev_player) # position of player
        names.append(names[0]) # to get 1st name if player was last
        player=names[position+1]
    else: player=prev_player

    return player

def add_row(self,hit,df):
    """Reads last row and creates another row with new score"""
    cplayer=get_current_player(df)
    last_row=df[df['name']==cplayer].tail(1) # last player row
    dart=last_row['dart'].values[0]
    cdart=int(dart+1) # current dart
    score=int(last_row['score'].values[0])
    pscore=int(last_row['pscore'].values[0]) # score in previous round
    score, next_player, next_leg=hit_evaluation(hit,score,pscore,cdart)

    rscore=pscore-score
    new_row=last_row
    if new_row['round'].values[0]==0:
        new_row['round']=int(1)

    new_row['dart']=cdart
    new_row['score']=score
    new_row_player_score=last_row['player_score'].values[0]

    new_row['rscore']=int(rscore)
    new_row['next_player']=next_player
    new_row['hit'+str(cdart)]=hit
    if next_player==True: # compute player score only if going to next player
         new_row['player_score']=new_row_player_score-rscore

    # legs=new_row['legs']
    if new_row['score'].values[0]==0:
        legs=int(new_row['legs'].values[0]+1)
        new_row['legs']=legs



    df=df.append(new_row,ignore_index=True)

    # new leg initiation:
    if next_leg==True: #and self.team_var.get()==0:
        print('next leg')
        sorted_names=last_score_rating(df)

        leg_df=gen_leg_table(df)
        export_to_csv(leg_df)  #calls function to export leg_df to csv
        export_to_csv(df) # calls function to export to csv for each new leg

        try:
            init_score=int(self.game.get())
        except: return

        for player in sorted_names:
            row0=df[df['name']==player].tail(1)
            row0['dart']=0
            for i in range(3):
                row0['hit'+str(i+1)]='' # reset all hits   

            row0['rscore']=0
            row0['score']=init_score
            row0['player_score']=init_score
            row0['pscore']=0
            row0['round']=0
            row0['next_player']=True
            newleg=row0['cleg'].values[0]+1
            row0['cleg']=newleg

            # update leg of second team player:
            if self.team_var.get()==1:
                team=row0['name'].values[0][1]
                if team in cplayer and player != cplayer:
                    row0['legs']=legs

            df=df.append(row0,ignore_index=True)


    # next player initiation:
    if next_player==True:
        nplayer=get_current_player(df)
        row0=df[df['name']==nplayer].tail(1)
        row0['dart']=0
        for i in range(3):
            row0['hit'+str(i+1)]='' # reset all hits   
        row0['rscore']=int(0)
        row0['pscore']=row0['score']
        # row0['player_score']=new_row_player_score
        cround=int(row0['round']) # current round (initially 0 for next player)
        row0['round']=cround+1
        row0['next_player']=False
        if score==0:
            score=init_score
        # update score of second team player:
        if self.team_var.get()==1:
            team=last_row['name'].values[0][1]
            names=self.selected_name_list
            # print('team: ', team)
            for name in names:
                if team in name and name != cplayer:
                    rteam=df[df['name']==name].tail(1).copy()
                    rteam['score']=score
                    df=df.append(rteam,ignore_index=True) # row with 2nd team player
            # export_to_csv(df)
        df=df.append(row0,ignore_index=True) # add row withnext player
        print(df.tail(4))
    return df

def hit2num(hit):
    if hit[0]=='T':
        hnum=3*int(hit[1:])
    elif hit[0]=='D':
        hnum=2*int(hit[1:])
    else: hnum=int(hit)
    return hnum

def hit_evaluation(hit,score,prscore,dart):
    next_player=True
    next_leg=False
    hnum=hit2num(hit)
    # print(hnum)
    if hnum==score:
        if hit[0]=='D':
            score=score-hnum
            print('score=',score, 'Finish')
            next_leg=True
        else:
            score=prscore
            print('no double')

    elif hnum<score-1:
        score=score-hnum
        print('ok, hit: '+str(hnum))

        if dart<3:
            next_player=False

    elif hnum>=score-1:
        print('overshot')
        score=prscore

    if next_player == True:
        dart=1

    else: dart=dart+1

    return score, next_player, next_leg

def last_score_rating(df):
    df2=df[df['round']==0]
    names=df2.sort_index(ascending=False).name.unique()
    # names=df.name.unique()
    score_data=pd.DataFrame()
    for name in names:
        last_row=df[df['name']==name].tail(1) # last player row
        score_data=score_data.append(last_row,ignore_index=True)

    sorted_names=score_data.sort_values(by='score',ascending=False)['name']

    # for team play:
    if names[0][2]==':': # for team play 
        s_names=score_data.sort_values(by='score',ascending=True)['name']
        ts=[] # initiation to get team 2,1,3,..

        for name in s_names: # best to worse
            if name[1] in ts: pass
            else:
                ts.append(name[1]) # append only unique team

        ts.reverse() # to get worst to best

        team_sorted_names=[]
        for i in range(len(names)):
            team_sorted_names.append('') # empty list of good size
        names_r=sorted_names.copy()
        names_r=names_r[::-1]

        for t, team in enumerate(ts):
            offset=0 # to add second player in same team
            for n, name in enumerate(names_r): # new team sequence:
                if name[1]==team:
                    team_sorted_names[t+offset]=name
                    offset=len(ts)
        sorted_names=team_sorted_names
    return sorted_names

def names_sequence(df):
    last_row=df.tail(1)
    cleg=last_row['cleg'].values[0]
    names= list(df[df['cleg']==cleg].name.unique()) # sorted names for this leg
    return names

def drop_last_row(self):
    df=self.data
    try:
        df_backup=self.backup
    except: df_backup=df.copy()
    last_row=df.tail(1)
    if last_row['hit1'].values[0]=='':
        df.drop(df.tail(2).index,inplace=True)
    else:
        df.drop(df.tail(1).index,inplace=True)
    return df,df_backup

def restore_row(df,df_backup):
    """restores hits which were removed by undo"""
    last_row=df.tail(1)

    last_row_id=df.index[-1]
    # print('lr df: '+str(last_row_id))
    rn=last_row_id+1
    if last_row['hit1'].values[0]=='' or last_row['hit2'].values[0]=='':
        df=df.append(df_backup.iloc[rn],ignore_index=True)
    else:
        df=df.append(df_backup.iloc[rn:rn+2],ignore_index=True)
    # print(df.tail(2))
    return df

def gen_leg_table(df):
    #df = pd.read_csv('Export/Score_20201208_2131.csv')
    initscore=df['score'].head(1).values[0]
    names=names_sequence(df)
    leg_df=pd.DataFrame()
    df_names=names.copy()
    df_names.append('closed by:')
    # print(df_names)
    leg_df['name']=df_names
    # leg_df['name']=names
    cleg=int(df['cleg'].tail(1).values[0])
    for leg in range(1,cleg+1,1): # start, stop, step, +1 to include current leg
        clegavg=[]
        for name in names: # going through all legs and names to get avg
            df0=df[df['next_player']==1]
            df1=df0[df0['name']==name]

            srow=df1[df1['cleg']==leg].tail(1)

            nrounds=srow['round'].values[0]
            if nrounds>0: # compute avg scoreonly for rounds bigger than 1
                score=srow['player_score'].values[0]
                avg=(initscore-score)/nrounds
                clegavg.append('%4.1f' %avg)
            else:
                clegavg.append('')

        # add closing round:
        try:
            cname=df['name'][df['cleg']==leg][df['score']==0].values[0]
            # print(cname)
            cround=df['round'][df['cleg']==leg][df['score']==0].values[0]
            # print('cround>  :')
            if cname[2]==':':
                close_str=cname[4:7]+'('+str(int(cround))+')'
            else:
                close_str=cname[:3]+'('+str(int(cround))+')'
            # print('closing name: '+close_str)
        except:
            close_str=''
        clegavg.append(close_str)

        leg_df[leg]=clegavg
    return leg_df

def sound_play(self):
    df=self.data
    eval_row=df.tail(1)

    if df.tail(1)['dart'].values[0]==0:
        eval_row=df.tail(2).head(1)

    last_hit=self.hit_str
    hits=[]
    for i in range(3):
        hit=eval_row['hit'+str(i+1)].values[0]
        hits.append(hit)


    # eval 26:
    if '1' in hits and '5' in hits and '20' in hits:
        print('ooh no')
        # mixer.init()
        mixer.music.load("sounds/DartCombo26.wav") #Loading File Into Mixer
        mixer.music.play() #Playing It In The Whole Device


    # eval triple miss (0 0 0):
    if hits[0]=='0' and hits[1]=='0' and hits[2]=='0':
        print('trippe miss')
        sound_dir='sounds/DartTripleMiss'
        playfile=random.choice(os.listdir(sound_dir))
        play_file_path=os.path.join(sound_dir,playfile)
        mixer.music.load(play_file_path) #Loading File Into Mixer
        mixer.music.play() #Playing It In The Whole Device
    # eval 115 (007 James Bond):

    if '1' in hits and '5' in hits and eval_row['rscore'].values[0]==7:
        print('James Bond 007')
        sound_dir='sounds/JamesBond'
        playfile=random.choice(os.listdir(sound_dir))
        play_file_path=os.path.join(sound_dir,playfile)
        mixer.music.load(play_file_path) #Loading File Into Mixer
        mixer.music.play() #Playing It In The Whole Device

    # eval 180:
    if eval_row['rscore'].values[0]==180:
        mixer.music.load("sounds/Dart180.wav") #Loading File Into Mixer
        mixer.music.play() #Playing It In The Whole Device

    # eval 100:
    if eval_row['rscore'].values[0]==100:
        mixer.music.load("sounds/Dart100.wav") #Loading File Into Mixer
        mixer.music.play() #Playing It In The Whole Device

def sound_play_start_game():
    mixer.music.load("sounds/DartGameStart.wav") #Loading File Into Mixer
    mixer.music.play() #Playing It In The Whole Device

def export_to_csv(df):
    # Create Export Directory if don't exist
    exp_d='Export'
    if not os.path.exists(exp_d):
        os.mkdir(exp_d)

    # file name
    dt=datetime.now()
    my_time_stamp=dt.strftime("%Y%m%d_%H%M")
    if len(df)>20: # name for all datas
        fname='Score_'+my_time_stamp+'.csv'
    else: #name for leg_df
        fname='Leg_avg_'+my_time_stamp+'.csv'
    fpath=os.path.join(exp_d,fname)

    #export to CSV:    
    df.to_csv(fpath)
